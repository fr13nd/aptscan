#!/bin/bash

# Color scheme variables
NC='\033[0m'              # Text Reset
RED='\033[0;31m'          # Red
GREEN='\033[0;32m'        # Green
YELLOW='\033[0;33m'       # Yellow
LB='\033[0;36m'           # Light Blue


function printUsage {
    echo "Usage: $0 -t google.com -w /usr/share/wordlists/dirb/common.txt"
    echo "OR"
    echo "Usage: $0 -l hostList.txt -w /usr/share/wordlists/dirb/common.txt"
    echo ""
    echo "Required arguments:"
    echo "      -w    |   Wordlists to use with gobuster"
    echo "AND"
    echo "      -t    |   Target to scan"
    echo "OR"
    echo "      -l    |   File with list of targets to scan"
}


# Start of program

#Process the command line arguments
if [ $# == 0 ]; then
    printUsage
    exit
fi

while [ "$1" != "" ]; do
    case $1 in
        -t | --target )         shift
                                TARGET=$1
                                ;;
        -w | --wordlist )       shift
                                WORDLIST=$1
                                ;;
        -l | --wordlist )       shift
                                INFILE=$1
                                ;;
        -h | --help )           printUsage
                                echo
                                exit
                                ;;
        * )                     echo -e "${RED}[!]${NC} Unknown option: $1"
                                echo
                                printUsage
                                exit
    esac
    shift
done

if [ "$TARGET" = "" ]; then
    ERROR=1
else
    lines=($TARGET)
fi
if [ "$INFILE" = "" ]; then
    ERROR=1
else
    IFS=$'\n' read -d '' -r -a lines < $INFILE
fi
if [ "$ERROR" = 2 ]; then
    echo -e "${RED}[!]${NC} ERROR: -t or -l was not supplied"
    echo
    printUsage
    exit
fi
if [ "$WORDLIST" = "" ]; then
    echo -e "${RED}[!]${NC} ERROR: -w was not supplied"
    echo
    printUsage
    exit
fi

# Loop over all hosts in the array and run all the commands on them
for host in "${lines[@]}"
do
    if [[ $host == *"https"* ]]; then
        PORT="443"
    else
        PORT="80"
    fi
    echo -e "Scanning: ${LB}$host${NC}"
    HOST_STRIP="$( echo "$host" | sed -e 's#https://##; s#http://##')"
    mkdir "$HOST_STRIP"
    cd "$HOST_STRIP"
    echo -e "${GREEN}[+] Scan Type: ${NC}dig on $host"
    dig "$host" | tee -a "dig_$HOST_STRIP-$PORT.txt"
    echo ""
    echo -e "${GREEN}[+] Scan Type: ${NC}nslookup on $host"
    nslookup "$HOST_STRIP" | tee -a "nslookup_$HOST_STRIP-$PORT.txt"
    echo ""
    echo -e "${GREEN}[+] Scan Type: ${NC}SSLyze on $host"
    #python -m sslyze --regular "$HOST_STRIP" >> "sslyze_$HOST_STRIP:$PORT.txt"
    python -m sslyze --regular "$HOST_STRIP" | tee -a "sslyze_$HOST_STRIP-$PORT.txt"
    echo ""
    echo -e "${GREEN}[+] Scan Type: ${NC}wafw00f on $host"
    #wafw00f -a "$host" | sed -e '1,12d' >> "wafw00f_$HOST_STRIP:$PORT.txt"
    wafw00f -a "$host" | sed -e '1,12d' | tee -a "wafw00f_$HOST_STRIP-$PORT.txt"
    echo ""
    echo -e "${GREEN}[+] Scan Type: ${NC}nitko on $host"
    nikto -nointeractive -maxtime 600 -host "$HOST_STRIP" | tee -a "nikto_$HOST_STRIP-$PORT.txt"
    echo ""
    echo -e "${GREEN}[+] Scan Type: ${NC}gobuster on $host"
    gobuster dir -u "$host" -w $WORDLIST -t 40 | tee -a "gobust_$HOST_STRIP-$PORT.txt"
    echo ""
    echo ""
    cd ..
done