# aptScan

Bash wrapper around tools used for Application Penetration Tests.

## Installation

Use the included setup.sh script to install dependencies.

```bash
./setup.sh
```

## Help
```bash
./aptScan.sh -h
Usage: ./aptScan.sh -t google.com -w /usr/share/wordlists/dirb/common.txt

Required arguments:
      -w    |   Wordlists to use with gobuster
AND
      -t    |   Target to scan
OR
      -l    |   File with list of targets to scan
```

## Usage

Scan a single URL

```bash
./aptScan.sh -t google.com -w /usr/share/wordlists/dirb/common.txt
```

Scan multiple URLs

```bash
./aptScan.sh -l hostList.txt -w /usr/share/wordlists/dirb/common.txt
```
